/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nikola
 */
public class DodajLet {

    private int idLeta;
    private String brojLeta, avioKompanija, OdGrada, DoGrada, datum, vreme, cenaKarte, klasa;

    public DodajLet() {
    }

    public DodajLet(String brojLeta, String avioKompanija, String OdGrada, String DoGrada, String datum, String vreme, String cenaKarte, String klasa) {
        this.brojLeta = brojLeta;
        this.avioKompanija = avioKompanija;
        this.OdGrada = OdGrada;
        this.DoGrada = DoGrada;
        this.datum = datum;
        this.vreme = vreme;
        this.cenaKarte = cenaKarte;
        this.klasa = klasa;
    }

    public int getIdLeta() {
        return idLeta;
    }

    public void setIdLeta(int idLeta) {
        this.idLeta = idLeta;
    }

    public String getBrojLeta() {
        return brojLeta;
    }

    public void setBrojLeta(String brojLeta) {
        this.brojLeta = brojLeta;
    }

    public String getAvioKompanija() {
        return avioKompanija;
    }

    public void setAvioKompanija(String avioKompanija) {
        this.avioKompanija = avioKompanija;
    }

    public String getOdGrada() {
        return OdGrada;
    }

    public void setOdGrada(String OdGrada) {
        this.OdGrada = OdGrada;
    }

    public String getDoGrada() {
        return DoGrada;
    }

    public void setDoGrada(String DoGrada) {
        this.DoGrada = DoGrada;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getVreme() {
        return vreme;
    }

    public void setVreme(String vreme) {
        this.vreme = vreme;
    }

    public String getKlasa() {
        return klasa;
    }

    public void setKlasa(String klasa) {
        this.klasa = klasa;
    }

    public String getCenaKarte() {
        return cenaKarte;
    }

    public void setCenaKarte(String cenaKarte) {
        this.cenaKarte = cenaKarte;
    }



}
