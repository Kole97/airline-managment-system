/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import baza.Baza;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import model.DodajLet;
import model.KorisnikRegistracija;

/**
 *
 * @author Nikola
 */
public class PlacanjePanel extends javax.swing.JPanel {

    /**
     * Creates new form NewJPanel1
     */
    public PlacanjePanel() {
        initComponents();
        validacijaUnosa();
        prikaziLetove();

    }

    public void validacijaUnosa() {
        broj_pasosa_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[0-9]{9}$");
                    Matcher m = p.matcher(getBroj_pasosa_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli broj, broj mora sadrzati 9 cifara!");
                        broj_pasosa_tf.setText("");
                    }
                }
            }
        }
        );
        broj_kartice_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$");
                    Matcher m = p.matcher(getBroj_kartice_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli broj kartice, unesite u formi [xxxx-xxxx-xxxx-xxxx]!");
                        broj_kartice_tf.setText("");
                    }
                }
            }
        }
        );

        tip_kartice_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[a-zA-Z]$");
                    Matcher m = p.matcher(getTip_kartice_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli tip kartice, moze sadrzati samo slova");
                        tip_kartice_tf.setText("");
                    }
                }
            }
        }
        );

        mesec_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[0-9]{2}$");
                    Matcher m = p.matcher(getMesec_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli mesec isteka kartice, moze sadrzati samo 2 broja");
                        mesec_tf.setText("");
                    }
                }
            }
        }
        );

        godina_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[0-9]{4}$");
                    Matcher m = p.matcher(getGodina_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli godinu isteka kartice, moze sadrzati samo 4 broja");
                        godina_tf.setText("");
                    }
                }
            }
        }
        );

        cvv_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[0-9]{3}$");
                    Matcher m = p.matcher(getCvv_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli sigurnosni kod kartice, moze sadrzati samo 3 broja");
                        cvv_tf.setText("");
                    }
                }
            }
        }
        );

        ime_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[a-zA-Z]+$");
                    Matcher m = p.matcher(getIme_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli ime, moze da sadrzi samo slova!");
                        ime_tf.setText("");
                    }
                }
            }
        }
        );

        prezime_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    Pattern p = Pattern.compile("^[a-zA-Z]+$");
                    Matcher m = p.matcher(getPrezime_tf().getText());

                    if (!m.matches()) {
                        JOptionPane.showMessageDialog(null, "Niste dobro uneli prezime, moze da sadrzi samo slova!");
                        prezime_tf.setText("");
                    }
                }
            }
        }
        );

        email_tf.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
            Pattern p = Pattern.compile("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$");
            Matcher m = p.matcher(getEmail_tf().getText());
            
            if (!m.matches()) {
                JOptionPane.showMessageDialog(null, "Unesite vas email u formi karakteri2-9@slova3-6.slova2");
                email_tf.setText("");
            }
        }
            }
        }
        );
    }

    public ArrayList<DodajLet> letLista() {
        ArrayList<DodajLet> letovi = new ArrayList<>();
        Connection baza = Baza.getConn();
        String upit = "select * from rezervacije";
        try {
            Statement st = baza.createStatement();
            ResultSet rs = st.executeQuery(upit);
            DodajLet dodajLet;
            while (rs.next()) {
                dodajLet = new DodajLet(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9));
                letovi.add(dodajLet);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return letovi;
    }

    public void prikaziLetove() {
        ArrayList<DodajLet> lista = letLista();
        DefaultTableModel model = (DefaultTableModel) tabelaLetovi.getModel();
        Object[] row = new Object[9];
        for (int i = 0; i < lista.size(); i++) {
            row[0] = lista.get(i).getBrojLeta();
            row[1] = lista.get(i).getAvioKompanija();
            row[2] = lista.get(i).getOdGrada();
            row[3] = lista.get(i).getDoGrada();
            row[4] = lista.get(i).getDatum();
            row[5] = lista.get(i).getVreme();
            row[6] = lista.get(i).getCenaKarte();
            row[7] = lista.get(i).getKlasa();
            model.addRow(row);

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        tekst_lb = new javax.swing.JLabel();
        ime_lb = new javax.swing.JLabel();
        prezime_lb = new javax.swing.JLabel();
        broj_kartice_lb = new javax.swing.JLabel();
        tip_kartice_lb = new javax.swing.JLabel();
        mesec_lb = new javax.swing.JLabel();
        godina_lb = new javax.swing.JLabel();
        cvv_lb = new javax.swing.JLabel();
        email_lb = new javax.swing.JLabel();
        ime_tf = new javax.swing.JTextField();
        prezime_tf = new javax.swing.JTextField();
        broj_kartice_tf = new javax.swing.JTextField();
        tip_kartice_tf = new javax.swing.JTextField();
        mesec_tf = new javax.swing.JTextField();
        godina_tf = new javax.swing.JTextField();
        cvv_tf = new javax.swing.JTextField();
        email_tf = new javax.swing.JTextField();
        plati_btn = new javax.swing.JButton();
        otkazi_btn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaLetovi = new javax.swing.JTable();
        korisnik_lb = new javax.swing.JLabel();
        slika_lb = new javax.swing.JLabel();
        broj_pasosa_lb = new javax.swing.JLabel();
        broj_pasosa_tf = new javax.swing.JTextField();
        /*  
        ime_tf.setText(k.getIme());
       prezime_tf.setText(k.getPrezime());
      email_tf.setText(k.getEmail());
         */
        setPreferredSize(new java.awt.Dimension(1400, 749));

        tekst_lb.setFont(new java.awt.Font("Segoe Print", 3, 24)); // NOI18N
        tekst_lb.setText("PLACANJE");

        ime_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        ime_lb.setText("Ime");

        prezime_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        prezime_lb.setText("Prezime");

        broj_kartice_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        broj_kartice_lb.setText("Broj kartice");
        broj_kartice_lb.setToolTipText("");

        tip_kartice_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        tip_kartice_lb.setText("Tip kartice");

        mesec_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        mesec_lb.setText("Mesec");

        godina_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        godina_lb.setText("Godina");

        cvv_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        cvv_lb.setText("CVV");

        email_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        email_lb.setText("Email");

        plati_btn.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        plati_btn.setText("PLATI");

        otkazi_btn.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        otkazi_btn.setText("OTKAZI");
        otkazi_btn.setToolTipText("");

        tabelaLetovi.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "Broj leta", "Avio kompanija", "Od grada", "Do grada", "Datum", "Vreme", "Cena karte", "Klasa"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                true, true, false, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelaLetovi);

        korisnik_lb.setFont(new java.awt.Font("Segoe Script", 3, 18)); // NOI18N

        slika_lb.setIcon(new javax.swing.ImageIcon("D:\\NetBeansProjects\\ZavrsniProjekat\\slike\\placanje.png")); // NOI18N

        broj_pasosa_lb.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        broj_pasosa_lb.setText("Broj pasosa");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(email_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(prezime_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(ime_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(broj_pasosa_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(55, 55, 55)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(broj_pasosa_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(prezime_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(ime_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(email_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 805, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(tip_kartice_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(broj_kartice_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(mesec_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(godina_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addComponent(cvv_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(plati_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(otkazi_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(tip_kartice_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(mesec_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(godina_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(cvv_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(broj_kartice_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(77, 77, 77)
                                                                .addComponent(slika_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(49, 49, 49))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(korisnik_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(225, 225, 225)
                                .addComponent(tekst_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(korisnik_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(tekst_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(32, 32, 32)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                        .addComponent(ime_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(ime_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(18, 18, 18)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                        .addComponent(prezime_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(prezime_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 22, Short.MAX_VALUE)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                .addComponent(email_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(0, 2, Short.MAX_VALUE)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addComponent(email_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGap(18, 18, 18)
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(broj_pasosa_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(broj_pasosa_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                .addGap(1, 1, 1))
                                                                        .addGroup(layout.createSequentialGroup()
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                                                        .addComponent(broj_kartice_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(broj_kartice_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                .addGap(27, 27, 27)
                                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                        .addComponent(tip_kartice_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                        .addComponent(tip_kartice_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                                .addGap(38, 38, 38)))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(mesec_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(mesec_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(18, 18, 18)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(godina_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(godina_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(26, 26, 26)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(cvv_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(cvv_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(slika_lb, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(plati_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(otkazi_btn, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(40, 40, 40))
        );

        setSize(1400, 749);
        setLocation(10, 0);
        setVisible(true);
    }// </editor-fold>                        

    // Variables declaration - do not modify                     
    private javax.swing.JLabel broj_kartice_lb;
    private javax.swing.JTextField broj_kartice_tf;
    private javax.swing.JLabel broj_pasosa_lb;
    private javax.swing.JTextField broj_pasosa_tf;
    private javax.swing.JLabel cvv_lb;
    private javax.swing.JTextField cvv_tf;
    private javax.swing.JLabel email_lb;
    private javax.swing.JTextField email_tf;
    private javax.swing.JLabel godina_lb;
    private javax.swing.JTextField godina_tf;
    private javax.swing.JLabel ime_lb;
    private javax.swing.JTextField ime_tf;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel korisnik_lb;
    private javax.swing.JLabel mesec_lb;
    private javax.swing.JTextField mesec_tf;
    private javax.swing.JButton otkazi_btn;
    private javax.swing.JButton plati_btn;
    private javax.swing.JLabel prezime_lb;
    private javax.swing.JTextField prezime_tf;
    private javax.swing.JLabel slika_lb;
    private javax.swing.JTable tabelaLetovi;
    private javax.swing.JLabel tekst_lb;
    private javax.swing.JLabel tip_kartice_lb;
    private javax.swing.JTextField tip_kartice_tf;
    // End of variables declaration                

    public JTextField getBroj_kartice_tf() {
        return broj_kartice_tf;
    }

    public JTextField getBroj_pasosa_tf() {
        return broj_pasosa_tf;
    }

    public JTextField getCvv_tf() {
        return cvv_tf;
    }

    public JTextField getEmail_tf() {
        return email_tf;
    }

    public JTextField getGodina_tf() {
        return godina_tf;
    }

    public JTextField getMesec_tf() {
        return mesec_tf;
    }

    public JButton getOtkazi_btn() {
        return otkazi_btn;
    }

    public JButton getPlati_btn() {
        return plati_btn;
    }

    public JTextField getTip_kartice_tf() {
        return tip_kartice_tf;
    }

    public void setEmail_tf(JTextField email_tf) {
        this.email_tf = email_tf;
    }

    public void setIme_tf(JTextField ime_tf) {
        this.ime_tf = ime_tf;
    }

    public void setPrezime_tf(JTextField prezime_tf) {
        this.prezime_tf = prezime_tf;
    }

    public JTextField getIme_tf() {
        return ime_tf;
    }

    public JTextField getPrezime_tf() {
        return prezime_tf;
    }

}
