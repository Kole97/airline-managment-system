
package baza;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Nikola
 */
public class Baza {

    private static Connection conn = null;
    private String url = "jdbc:mysql://localhost:3306/rezervacija_karata";
    private String user = "root";
    private String pass = "kole97";
    
    private static Baza instanca =null;

    private Baza() {
        try {
            conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException ex) {
            Logger.getLogger(Baza.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public static Connection getConn(){
        if(instanca ==null){
            instanca = new Baza();
        }
        return instanca.conn;
    
    }
}
