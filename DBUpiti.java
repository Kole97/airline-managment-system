package baza;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.DodajLet;
import model.KorisnikRegistracija;
import model.Placanje;

public class DBUpiti {
//static KorisnikRegistracija k1;

    public static void kreirajTabeluKorisnik() {

        String upit = "create table if not exists korisnik (korinsik_id int auto_increment not null primary key, ime varchar(20), prezime varchar(20), email varchar(20), korisnicko_ime varchar(20), sifra varchar(20))";

        Connection baza = Baza.getConn();

        try {
            Statement st = baza.createStatement();
            st.execute(upit);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public static void dodajKorisnika(KorisnikRegistracija k) {
        Connection baza = Baza.getConn();
        String upit = "insert into korisnik (ime, prezime,email, korisnicko_ime, sifra) values (?,?,?,?,?)";
        try {
            PreparedStatement pst = baza.prepareStatement(upit, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, k.getIme());
            pst.setString(2, k.getPrezime());
            pst.setString(3, k.getEmail());
            pst.setString(4, k.getKorisnickoIme());
            pst.setString(5, k.getSifra());
            pst.execute();

            //ovom metodom povlacimo sve generisane kljuceve
            ResultSet rs = pst.getGeneratedKeys();

            if (rs.next()) {
                int id = rs.getInt(1);
                k.setKorisnik_id(id);
                JOptionPane.showMessageDialog(null, "Uspesno ste uneli korisnika u bazu!!!\n" + "Korisnik je dobio ID " + k.getKorisnik_id());
                //System.out.println(k);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBUpiti.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static boolean proveriKorisnika(KorisnikRegistracija k) {
        Connection baza = Baza.getConn();

        String upit = "select * from korisnik where korisnicko_ime= ? and sifra= ?";
        try {
            PreparedStatement pst = baza.prepareStatement(upit, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, k.getKorisnickoIme());
            pst.setString(2, k.getSifra());
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBUpiti.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
         public static KorisnikRegistracija ucitajKorisnika(KorisnikRegistracija k,String korisnickoIme){
            Connection baza= Baza.getConn();
            
        String upit = "select * from korisnik where korisnicko_ime = '" + k.getKorisnickoIme()+ "';";
        try {
            PreparedStatement pst = baza.prepareStatement(upit);
   
            ResultSet rs = pst.executeQuery();

           
            if (rs.next()) {
               // k.setId(rs.getInt(1));
                k.setIme(rs.getString(2));
                k.setPrezime(rs.getString(3));
             //   k.setKorisnickoIme(rs.getString(4));
              //  k.setLozinka(rs.getString(5));
               // k.setBrTelefon(rs.getString(6));
               // k.setEmail(rs.getString(7));
                return k;
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBUpiti.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void kreirajTabeluLet() {

        String upit = "create table if not exists let (id_leta int auto_increment not null primary key, broj_leta varchar(20),avio_kompanija varchar(20), od_grada varchar(20), do_grada varchar(20), datum varchar(20), vreme varchar(20), cena_karte varchar(20), klasa varchar(20))";

        Connection baza = Baza.getConn();

        try {
            Statement st = baza.createStatement();
            st.execute(upit);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public static void dodajLet(DodajLet l ) {
        Connection baza = Baza.getConn();
        String upit = "insert into let (broj_leta,avio_kompanija,od_grada, do_grada, datum, vreme, cena_karte, klasa) values (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = baza.prepareStatement(upit, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, l.getBrojLeta());
            pst.setString(2, l.getAvioKompanija());
            pst.setString(3, l.getOdGrada());
            pst.setString(4, l.getDoGrada());
            pst.setString(5, l.getDatum());
            pst.setString(6, l.getVreme());
            pst.setString(7, l.getCenaKarte());
            pst.setString(8, l.getKlasa());

            pst.execute();

            //ovom metodom povlacimo sve generisane kljuceve
            ResultSet rs = pst.getGeneratedKeys();

            if (rs.next()) {
                int id = rs.getInt(1);
                l.setIdLeta(id);
                JOptionPane.showMessageDialog(null, "Uspesno ste uneli let u bazu!");
                System.out.println(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBUpiti.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
        public static void kreirajTabeluRezervacija() {

        String upit = "create table if not exists rezervacije (id_leta int auto_increment not null primary key, broj_leta varchar(20),avio_kompanija varchar(20), od_grada varchar(20), do_grada varchar(20), datum varchar(20), vreme varchar(20), cena_karte varchar(20), klasa varchar(20))";

        Connection baza = Baza.getConn();

        try {
            Statement st = baza.createStatement();
            st.execute(upit);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
        
         public static void dodajRezervaciju(DodajLet l ) {
        Connection baza = Baza.getConn();
        String upit = "insert into rezervacije (broj_leta,avio_kompanija,od_grada, do_grada, datum, vreme, cena_karte, klasa) values (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = baza.prepareStatement(upit, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, l.getBrojLeta());
            pst.setString(2, l.getAvioKompanija());
            pst.setString(3, l.getOdGrada());
            pst.setString(4, l.getDoGrada());
            pst.setString(5, l.getDatum());
            pst.setString(6, l.getVreme());
            pst.setString(7, l.getCenaKarte());
            pst.setString(8, l.getKlasa());

            pst.execute();

            //ovom metodom povlacimo sve generisane kljuceve
            ResultSet rs = pst.getGeneratedKeys();

            if (rs.next()) {
                int id = rs.getInt(1);
                l.setIdLeta(id);
                JOptionPane.showMessageDialog(null, "Uspesno ste rezervisali Vas let :)");
                //System.out.println(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBUpiti.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public static void kreirajTabeluUplate() {

        String upit = "create table if not exists uplate  (id int auto_increment not null primary key, ime varchar(20), prezime varchar(20), email varchar(20), broj_pasosa varchar(20), broj_kartice varchar(20),tip_kartice varchar(20), mesec varchar(20), godina varchar(20), cvv varchar(20))";

        Connection baza = Baza.getConn();

        try {
            Statement st = baza.createStatement();
            st.execute(upit);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
    
      public static void dodajUplatu(Placanje p ) {
        Connection baza = Baza.getConn();
        String upit = "insert into uplate (ime,prezime,email, broj_pasosa, broj_kartice, tip_kartice, mesec, godina,cvv) values (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = baza.prepareStatement(upit, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, p.getIme());
            pst.setString(2, p.getPrezime());
            pst.setString(3, p.getEmail());
            pst.setString(4, p.getBrojPasosa());
            pst.setString(5, p.getBrojKartice());
            pst.setString(6, p.getTipKartice());
            pst.setString(7, p.getMesec());
            pst.setString(8, p.getGodina());
            pst.setString(9, p.getCvv());

            pst.execute();

            //ovom metodom povlacimo sve generisane kljuceve
            ResultSet rs = pst.getGeneratedKeys();

            if (rs.next()) {
                int id = rs.getInt(1);
                p.setId(id);
                JOptionPane.showMessageDialog(null, "Uspesno ste uplatili Vas let :)");
                //System.out.println(l);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DBUpiti.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
