/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import baza.DBUpiti;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JOptionPane;
import model.DodajLet;
import model.KorisnikRegistracija;
import model.Placanje;
import view.DodajLetPanel;
import view.InformacijeOLetuPanel;
import view.InformacijeOLetuPanel1;
import view.NakonPrijavljivanjaOkvir;
import view.PlacanjePanel;
import view.PocetniOkvir;
import view.PrijavljivanjePanel;
import view.RegistracijaPanel;
import view.UplatePanel;

public class Kontroler {

    private PocetniOkvir okvir;
    private PrijavljivanjePanel prijavljivanje_panel;
    private RegistracijaPanel panel;
    private DodajLetPanel dodajLetPanel;
    private InformacijeOLetuPanel panelInf;
    private InformacijeOLetuPanel1 panelInf1;
    private PlacanjePanel placanjePanel;
    private NakonPrijavljivanjaOkvir okvir1;
    private KorisnikRegistracija model;
    private UplatePanel uplatePanel;

    public Kontroler(PocetniOkvir okvir) {
        this.okvir = okvir;
        dodajSlusacPrijavljivanje();
        dodajSlusacRegistracija();

    }

    private void dodajSlusacRegistracija() {
        okvir.getRegistrujtese_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                // System.out.println("desio se klik na reg bt");
                okvir.getPocetniPanel().setVisible(false);
                RegistracijaPanel panel1 = new RegistracijaPanel();
                okvir.add(panel1);

                dodajSlusacRegistrujSe(panel1);

            }
        }
        );

    }

    private void dodajSlusacRegistrujSe(RegistracijaPanel panel1) {

        panel1.getRegistruj_se_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                // System.out.println("Desio se klik na reg baton");
                try {
                    KorisnikRegistracija k = new KorisnikRegistracija(panel1.getIme_tf().getText(), panel1.getPrezime_tf().getText(), panel1.getKorisnicko_ime_tf().getText(), panel1.getLozinka_tf().getText(), panel1.getEmail_tf().getText());
                    DBUpiti.kreirajTabeluKorisnik();
                    DBUpiti.dodajKorisnika(k);
                    panel1.setVisible(false);
                    okvir.getPocetniPanel().setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        );
    }

    private void dodajSlusacPrijavljivanje() {
        okvir.getUlogujte_se_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
               // System.out.println("desio se klik na log bt");
                okvir.getPocetniPanel().setVisible(false);
                PrijavljivanjePanel panel2 = new PrijavljivanjePanel();
                okvir.add(panel2);
                dodajSlusacPrijaviSe(panel2);
            }

        }
        );

    }

    private void dodajSlusacPrijaviSe(PrijavljivanjePanel panel2) {
        panel2.getUloguj_se_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
              //  System.out.println("Desio se klik na logujse bt");
                String korisnickoIme = panel2.getKorisnicko_ime_tf().getText();
                String sifra = panel2.getSifra_pf().getText();
                KorisnikRegistracija k = new KorisnikRegistracija();
                k.setKorisnickoIme(korisnickoIme);
                k.setSifra(sifra);

                boolean ok = DBUpiti.proveriKorisnika(k);

                if (ok) {
                    if (korisnickoIme.equals("kole") && sifra.equals("kole97")) {
                        JOptionPane.showMessageDialog(null, "Uspesno ste se ulogovali kao administrator!");
                        okvir.dispose();
                        okvir1 = new NakonPrijavljivanjaOkvir();
                        dodajSlusacDodajLet(okvir1);
                        dodajSlusacInformacijeOLetu(okvir1);
                        dodajSlusacUplate(okvir1);
                        

                    } else {
                        JOptionPane.showMessageDialog(okvir, "Uspesno ste se ulogovali!");
                        panel2.setVisible(false);
                        panelInf = new InformacijeOLetuPanel();
                        DBUpiti.ucitajKorisnika(k, korisnickoIme);
                        panelInf.getKorisnik_lb().setText("Korisnik : " + k.getIme() + " " + k.getPrezime());
                        okvir.add(panelInf);
                        dodajSlusacRezervisi(panelInf);
                        dodajSlusacPonisti(panelInf);

                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Greska, probajte ponovo!");
                    panel2.getKorisnicko_ime_tf().setText("");
                    panel2.getSifra_pf().setText("");
                }
               
            }
        }
        );
    }

    private void dodajSlusacDodajLet(NakonPrijavljivanjaOkvir okvir1) {
        okvir1.getDodaj_let_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                okvir1.getNakonPrijavljivanjaPanel().setVisible(false);
                dodajLetPanel = new DodajLetPanel();
                okvir1.add(dodajLetPanel);
                dodajSlusacSacuvaj(dodajLetPanel);
                dodajSlusacPonisti(dodajLetPanel);
            }

        }
        );

    }

    private void dodajSlusacInformacijeOLetu(NakonPrijavljivanjaOkvir okvir1) {
        okvir1.getInformacije_o_letu_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                okvir1.getNakonPrijavljivanjaPanel().setVisible(false);
                panelInf1 = new InformacijeOLetuPanel1();
                okvir1.add(panelInf1);
                dodajSlusacPonisti(panelInf1);
            }

        }
        );

    }

    private void dodajSlusacUplate(NakonPrijavljivanjaOkvir okvir1) {
        okvir1.getUplate_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                okvir1.getNakonPrijavljivanjaPanel().setVisible(false);
                uplatePanel = new UplatePanel();
                okvir1.add(uplatePanel);
                dodajSlusacVratiSe(uplatePanel);
            }

        }
        );
    }
    
    private void dodajSlusacVratiSe(UplatePanel uplatePanel){
        uplatePanel.getVrati_se_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                uplatePanel.setVisible(false);
                okvir1.getNakonPrijavljivanjaPanel().setVisible(true);
                       
            }
            
        }
        );
    }

    private void dodajSlusacPonisti(InformacijeOLetuPanel1 panelInf1) {
        panelInf1.getPonisti_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                panelInf1.setVisible(false);
                okvir1.getNakonPrijavljivanjaPanel().setVisible(true);
            }

        }
        );
    }

    private void dodajSlusacPonisti(DodajLetPanel dodajLetPanel) {
        dodajLetPanel.getPonisti_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                // System.out.println("Desio se klik na sac baton");
                dodajLetPanel.setVisible(false);
                okvir1.getNakonPrijavljivanjaPanel().setVisible(true);

            }
        }
        );
    }

    private void dodajSlusacSacuvaj(DodajLetPanel dodajLetPanel) {

        dodajLetPanel.getSacuvaj_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                // System.out.println("Desio se klik na sac baton");
                try {
                    DodajLet l = new DodajLet(dodajLetPanel.getBr_leta_tf().getText(), dodajLetPanel.getAvio_komp_tf().getText(), dodajLetPanel.getOd_grada_tf().getText(), dodajLetPanel.getDo_grada_tf().getText(), dodajLetPanel.getDatum_tf().getText(), dodajLetPanel.getVreme_tf().getText(), dodajLetPanel.getCena_karte_tf().getText(), dodajLetPanel.getKlasa_tf().getText());
                    DBUpiti.kreirajTabeluLet();
                    DBUpiti.dodajLet(l);
                    dodajLetPanel.setVisible(false);
                    okvir1.getNakonPrijavljivanjaPanel().setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        );
    }

    private void dodajSlusacRezervisi(InformacijeOLetuPanel panelInf) {
        panelInf.getRezervisi_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                try {
                    DodajLet l = new DodajLet(panelInf.getBr_leta_tf().getText(), panelInf.getAvio_komp_tf().getText(), panelInf.getOd_grada_tf().getText(), panelInf.getDo_grada_tf().getText(), panelInf.getDatum_tf().getText(), panelInf.getVreme_tf().getText(), panelInf.getCena_karte_tf().getText(), panelInf.getKlasa_tf().getText());
                    DBUpiti.kreirajTabeluRezervacija();
                    DBUpiti.dodajRezervaciju(l);
                    panelInf.setVisible(false);
                    placanjePanel = new PlacanjePanel();
                    okvir.add(placanjePanel);
                    dodajSlusacUplati(placanjePanel);
                    dodajSlusacOtkazi(placanjePanel);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
        );

    }

    private void dodajSlusacPonisti(InformacijeOLetuPanel panelInf) {
        panelInf.getPonisti_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                panelInf.setVisible(false);
                okvir.getPocetniPanel().setVisible(true);
            }

        }
        );
    }

    private void dodajSlusacUplati(PlacanjePanel placanjePanel) {
        placanjePanel.getPlati_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                int odgovor = JOptionPane.showConfirmDialog(placanjePanel, "Da li sigurno zelite da izvrsite uplatu?", "Potvrdi", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (odgovor == JOptionPane.YES_OPTION) {
                    Placanje p = new Placanje(placanjePanel.getIme_tf().getText(), placanjePanel.getPrezime_tf().getText(), placanjePanel.getEmail_tf().getText(), placanjePanel.getBroj_pasosa_tf().getText(), placanjePanel.getBroj_kartice_tf().getText(), placanjePanel.getTip_kartice_tf().getText(), placanjePanel.getMesec_tf().getText(), placanjePanel.getGodina_tf().getText(), placanjePanel.getCvv_tf().getText());
                    DBUpiti.kreirajTabeluUplate();
                    DBUpiti.dodajUplatu(p);
                    //JOptionPane.showMessageDialog(null, "Uspesno ste uplatili Vas let!");
                }
            }

        }
        );

    }

    private void dodajSlusacOtkazi(PlacanjePanel placanjePanel) {
        placanjePanel.getOtkazi_btn().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent me) {
                placanjePanel.setVisible(false);
                okvir.getPocetniPanel().setVisible(true);
            }

        }
        );

    }

}
