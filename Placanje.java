/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nikola
 */
public class Placanje {
    private int id;
    private String ime,prezime,email,brojPasosa,brojKartice,tipKartice,mesec,godina,cvv;

    public Placanje() {
    }

    public Placanje(String ime, String prezime, String email, String brojPasosa, String brojKartice, String tipKartice, String mesec, String godina, String cvv) {
        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.brojPasosa = brojPasosa;
        this.brojKartice = brojKartice;
        this.tipKartice = tipKartice;
        this.mesec = mesec;
        this.godina = godina;
        this.cvv = cvv;
    }

    public int getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getEmail() {
        return email;
    }

    public String getBrojPasosa() {
        return brojPasosa;
    }

    public String getBrojKartice() {
        return brojKartice;
    }

    public String getTipKartice() {
        return tipKartice;
    }

    public String getMesec() {
        return mesec;
    }

    public String getGodina() {
        return godina;
    }

    public String getCvv() {
        return cvv;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}
